/label ~Frontend ~"trainee maintainer"
/assign `@manager`

<!-- Congratulations! Fill out the following MR when you feel you are ready to become -->
<!-- a frontend maintainer! This MR should contain updates to `data/team.yml` -->
<!-- declaring yourself as a maintainer of `gitlab-org/gitlab-ui` and -->
<!-- `gitlab-org/gitlab-com` -->

## Self-assessment

It's hard to specify hard requirements for becoming a maintainer, which is why [the documentation](https://about.gitlab.com/handbook/engineering/workflow/code-review/#how-to-become-a-project-maintainer) consists of flexible guidelines. Nevertheless, it's still important to have a more formal list to help both a candidate and their manager to assess the "readiness" of the candidate.

- [ ]  Advanced understanding of the GitLab project as per [the documentation](https://about.gitlab.com/handbook/engineering/workflow/code-review/#how-to-become-a-project-maintainer):
  > good feel for the project codebase, expertise in one or more domains, and deep understanding of our coding standards
- [ ] The MRs reviewed by the candidate consistently make it through maintainer review without significant additionally required changes.
- [ ] The MRs authored by the candidate consistently make it through reviewer and maintainer review without significant required changes.

## Links to Non-Trival MRs I've Reviewed

<interesting MRs reviewed and discussions in trainee issue here>

## Links to Non-Trivial MRs I've Written

<interesting MRs authored here>

@gitlab-org/maintainers/frontend please chime in below with your thoughts, and
approve this MR if you agree.

## Once This MR is Merged

1. [ ] Create an [access request][access-request]
       for maintainer access to `gitlab-org/<project>`. <!-- make sure to update the <project> as needed, for example `gitlab-org/gitlab` -->
1. [ ] Join the [`[at]frontend-maintainers` slack group][frontend-maintainers-slack-group]
1. [ ] Let a maintainer add you to `gitlab-org/maintainers/frontend`
1. [ ] Announce it _everywhere_
1. [ ] Keep reviewing, start merging :sign_of_the_horns: :sunglasses: :sign_of_the_horns:

[access-request]: https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/#individual-or-bulk-access-request
[frontend-maintainers-slack-group]: https://gitlab.slack.com/archives/C9Q5V0597
