---
layout: handbook-page-toc
title: Coupa FAQ
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc}

- TOC
{:toc}

## GENERAL FAQ

### What is Coupa?

- Coupa is a cloud-based purchasing and payment platform that will be used by GitLab as from June 1st 2021 for US and Netherlands. It has an easy-to-use interface that will improve the way suppliers connect with GitLab. All new purchase orders, invoices and communications will be managed through the Coupa Supplier Portal.

### Does the Coupa implementation impact all GitLab Entities?

- This initial delivery is for US and Netherlands entities **only** with a go-live scheduled for June 1st 2021.

### Can I continue to transact like I do today after the Coupa go live? (e.g. via GitLab issues)

- You **will not** be able to transact as you do today after the Coupa Go-Live for the US and Netherlands entities.  
Once we're live (June 1st), all new Purchase Orders and associated invoices will flow through Coupa. 

### Do we still need to use GitLab issues after Go Live, if receiving POs and invoices from other entities?

- Yes. We will be rolling out in a phased approach, starting with the US and Netherlands. For all other entities the process will remain the same as is.

## SUPPLIER FAQ

### How will Coupa impact the way GitLab works with suppliers?

- To take full advantage of the platform and enable electronic transactions to flow to and from suppliers, selected suppliers will be enabled in Coupa in time for go-live at no cost to them. The Coupa Supplier Portal is a free tool for our suppliers to use and easily do business with GitLab.

### Are suppliers required to sign up with Coupa?

- Yes. When live, Coupa will be the only platform for transacting with GitLab, with all new Purchase Orders and associated invoices flowing through Coupa. For suppliers who choose to use the Coupa Supplier Portal (CSP), it is a free tool that allows to easily do business with GitLab.

### How will suppliers be notified when a PO has been raised for them?

- Depending on the preferred method of transacting:
   - SAN supplier will receive a PO emailed to their inbox
   - CSP suppliers will receive notifications via the CSP platform (supplier has multiple options on how to set up notifications: email, SMS)
   - cXML supplier will be notified in their own system as per their setup and they will receive notification in CSP.

## CSP/SAN FAQ

### What is the Coupa Supplier Portal (CSP)?

- The Coupa Supplier Portal (CSP) is a portal which enables GitLab's suppliers to receive purchase orders and create invoices electronically. Suppliers can also create and update catalogues through the CSP. 

### What are the key benefits of joining the CSP?

  - CSP makes managing customers and transactions easy.
  - Roll out world class processes and best-in-class content management.
  - Manage settings on a customer-by-customer basis, including viewing purchase orders, setting up delivery methods, creating catalogs etc.
  - Tracking the real time status of transactions.

### How long does it take to register for the CSP?

- The registration process takes less than 5 minutes upon receiving the invitation email. Suppliers can expect to receive an invitation email from GitLab on May 28th 2021.

### Do suppliers need additional software to use the CSP?

- No, additional software is **not** required. As Coupa is a cloud-based technology, all they will need is an active email account and web browser to access the portal and transact with GitLab.

### How much does it cost to transact with GitLab via the CSP?

- There is no cost for suppliers to use Coupa.

### How do suppliers register for the CSP?

- The supplier will receive an email invitation to join the CSP and GitLab will partner with them to ensure a smooth transition to Coupa. 

### If the supplier already have a Coupa Supplier Portal account, what do they need to do to enable GitLab?

- We still need the supplier to reply to our initial email so we can set their company up properly in our Coupa system. Once setup, they will be able to easily link GitLab to their Coupa Supplier Portal account. 

### Where can I find more information on the Coupa Supplier Portal?

- Here are some useful links with information on the Coupa Supplier Portal: 
   - [https://success.coupa.com/Suppliers/For_Suppliers/Coupa_Supplier_Portal](https://success.coupa.com/Suppliers/For_Suppliers/Coupa_Supplier_Portal) 
   - [https://www.coupa.com/suppliers/#faq](https://www.coupa.com/suppliers/#faq) 
   - [https://www.coupa.com/suppliers/#faq](https://supplier.coupa.com/help/)

### Do suppliers need to set-up a CSP account, or will this be done for them?

- GitLab will send the supplier links on how to register on the Coupa Supplier Portal and they will be provided with an invitation to join CSP in May, 2021. Suppliers must set up an CSP account and linking with GitLab Coupa system will be done automatically.

### Does SAN option connect to the CSP and can suppliers have both?

- Yes, suppliers can have both SAN and CSP set up. CSP suppliers will have SAN by default. Please note, suppliers who have initially chosen SAN will have an option to join CSP through the PO email sent.

## cXML OPTION FAQ

### Do suppliers receive purchase orders via e-mail if they select cXML as the transmission method?

- If they select cXML as your transmission method, they will not receive purchase orders via e-mail. The cXML links directly between the supplier's system and GitLab's system.
