---
layout: handbook-page-toc
title: Team Member Social Media Advocacy
description: Strategies and details to enable team members to share GitLab-related news on personal social media channels
twitter_image: /images/opengraph/handbook/social-marketing/social-handbook-top.png
twitter_image_alt: GitLab's Social Media Handbook branded image
twitter_site: gitlab
twitter_creator: gitlab
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## ⚠️ `THIS HANDBOOK PAGE IS UNDER CONSTRUCTION AND IS NOT CONSIDERED TO BE LIVE ADVICE AT THIS TIME` Please reach out to #social_media_action Slack channel with any questions

## Team Member Social Media Advocacy and Enablement

[According to Sprout Social](https://sproutsocial.com/insights/what-is-employee-advocacy/), team member advocacy is the promotion of an organization by its staff members. People trust recommendations and content from people they know. They trust these people a lot more than they trust marketing messages from companies on organic brand social channels or paid social media advertising. We know this when a team member can post the same content the brand channel publishes, but because the message was more personal, more human, the team member gains significant engagement over the brand channel.

### Social Advocacy Strategy

To enable all GitLab team members to confidently and comfortably share GitLab related stories on their personal social media channels in a way that is risk adverse yet provides measureable performance. 

#### Goals of social media advocacy 

- Improve the organic reach of GitLab messaging
- Increase traffic to our site from social media, particularly to blogs and campaign content
- Reduce brand and team member risk by providing curated content to publish
- Secondary: aid in sales processes for social selling initiatives 
- Secondary: drive recruitment to accelerate hiring values-drive team members

#### Benefits of social advocacy to all team members

- Curated and prewritten drafts for social posts allow for easy publishing with minimal edits required
- Posts can be scheduled, like a social media manager would for the brand, allowing the work to be semi-automated
- Easy way to include incentives like swag, bonuses, or other add-ons
- Career growth: sharing industry content on social media can help make team members be looked at as thought leaders in their spaces - this boosts your personal brand
- Save time - by checking Bambu on a regular cadence and scheduling content on your channels for a week or two, you'll be enabled to share a lot of GitLab stories without the need to spend a lot of time sourcing, writing, and manually posting on your own social channels

### Bambu, our social advocacy tool

Bambu by Sprout Social is an employee advocacy platform for you to share content across LinkedIn, Twitter or Facebook. Bambu enables you to quickly and easily share content on LinkedIn, Twitter and Facebook to amplify our brand reach and help establish your personal brand on social media.The goal of this tool is to centralize content that is valuable to our audience.

<details><summary>How do I get started?</summary>

You'll need to log into Okta and locate the Bambu logo tile. Click on the Okta tile and you'll automatically be logged in for the first time. You'll be promoted to confirm a few items and that's it! You'll always have access to Bambu via logging in with Okta. 

Once you've logged in for the first time, save a bookmark in your browser for https://gitlab.getbambu.com/login

Click the log in with SSO option at the bottom of the page

Follow the prompts and you'll be logged in quickly

</details>

<details><summary>What am I supposed to do inside Bambu?</summary>

When you log into Bambu, you will see a collection of stories curated specifically for you. This is a centralized hub for you to learn, build your reputation online, and help spread the word about GitLab by sharing these stories with your networks on Facebook, LinkedIn, and Twitter. We made it easy to share, too. When you click the share icon on any story, you’ll see that we’ve added a suggested status update. Of course, you’re welcome to adjust or completely rewrite it to match your own voice (the only exception to this will be when you’re sharing content that needs to be worded a certain way for compliance reasons, in which case, we'll outline in the notes).

</details>

<details><summary>How often am I supposed to login to Bambu?</summary>

As often as you like, but try to make a habit of logging in at least once a week, as we are constantly adding new and useful information. We will make sure that any time-sensitive stories find their way to you through Slack, like in the #whats-happening-at-gitlab channel, and there is also an email digest that can show you anything you’ve missed since the last time you logged in. If you use the scheduling posts feature, you could spend only 30 minutes or less per week lining up your social post calendar and letting Bambu do the rest.

Consider setting a reminder in Slack to check Bambu once a week or once every two weeks. 

</details>

#### Team Member Roles in Bambu, the advocacy program

<details><summary>Admins</summary>

<p>
The GitLab Social Team are the administrators of the social advocacy program. Admins have all access to our tool, Bambu, as needed to operate the program. Admins may act as curators from time to time as well.
</p>
<p>
 If you have questions or would like to learn more, consider sending a message in the #social-advocacy Slack channel.
</p>

<table>
  <tr>
   <td>Name
   </td>
   <td>Role
   </td>
  </tr>
  <tr>
   <td>Kristen Sundberg
   </td>
   <td>Curator and Program Admin
   </td>
  </tr>
  <tr>
   <td>Wil Spillane
   </td>
   <td>Technology Owner and Admin
   </td>
  </tr>
</table>

</details>

<details><summary>Curators (or Contributors)</summary>

<p>
Curators are selected intentionally to drive our advocacy content strategy and specific team members are asked to take on the role of a curator as a part of their everyday jobs at GitLab. We will have curators representing all areas of GitLab the brand and the product in order to curate a list of related content worth sharing on social media. 
</p>
<p>
Curators have all Reader access as well as the ability to curate stories and submit them to a Manager or Admin for approval. 
</p>
<p>
Join the #social-advocacy-curators Slack channel to stay in touch with the curator program and the latest news. This channel is intended for team members who are identified as content curators only.
</p>
<p>
<a href="https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/social-advocacy-curators/">If you're a curator or interested in becoming a curator, head to our curator-specific handbook page here.</a>
</p>

</details>

<details><summary>General Team Members (or Readers)</summary>

<p>
All team members can access the Stories feed, share to their social networks and leverage the Suggestions feature to submit links to a Manager or Admin for complete Story curation.
</p>
<p>
Access to Bambu is provided via an Okta tile - please log in to Okta and find the Bambu logo tile to Log on.
</p>
<p>
Join the #social-advocacy Slack channel to stay in touch with the program and the latest news.
</p>

</details>

### Suggesting content for team members to share on Bambu

While we have a team of content curators working to add relevant and fresh content for the whole team to share on a regular basis, you may come across content that you'd like us to consider adding to Bambu, mainly content from 3rd party sources: your own blogs or a partner website link. Here's how to suggest content to an admin to add to Bambu. 

- Click the +Suggest Story button in the upper right hand corner
- Paste the story URL and click Suggest Story
- The admin team will review your suggestion. If we approve it, we'll mark you as the curator for the content.

Please note that most of what is approrpriate to share that comes from GitLab will be curated already.

### Integrations

- We are actively working to include link attribution for Salesforce and Marketo, to better identify how shared content on social channels enabled the sales process.
- The Slack integration will allow us to broadcast a Bambu story link to Slack, giving our team the headsup on big news items that we'd like to share. This will allow GitLab to continue operating like GitLab. 
- Okta integration, as mentioned above, will be critical for security and self-service

### Contests

We'll run contests for team members from time to time to enable more sharing across particular topics or campaigns. You'll find out about these contests in the #whats-happening-at-gitlab Slack channel.

### Reporting and Metrics 

[Bambu provides a report center to outline all of the possibilities here](https://bambu.zendesk.com/hc/en-us/articles/360020038351-Bambu-Report-Guide).

Conversion Rate - the percentage of team members invited to the program that are actually participating

Active Participation - the percentage of team members that are engaged in the program and sharing on any given reporting period (week/month/quarter)

Top Contributors identifying top contributors helps to understand the kind of content that will work best and recognizing top contributors is a good way to keep the program engaging

Organic reach - the number of people seeing content shared through Bambu by our team member advocates

Engagement - measuring the number of actions taken on content shared through Bamby by our team member advocates (think likes, clicks, comments, and shares)

Ad value or equivalency - similar to how we measure this for the brand, this is measured in a dollar value for advertising determined by the sum of a reporting period's equivalent CPM + CPC costs

### FAQs

<details><summary>Why are preview images, titles, and text not included within shared posts?</summary>

There may be an issue with the page you're trying to use for the story. Preview images, titles, and text are dictated by the data in the frontmatter of the page. <a href="https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/admin/#data-needed-on-pages-in-order-for-links-to-work-on-social-media">Learn more about how to determine what information gets pulled by social channels here.</a>


</details>
