---
layout: handbook-page-toc
title: Social Advocacy Curator Program
description: Strategies and details to enable curators to share GitLab-related news for company-wide enablement
twitter_image: /images/opengraph/handbook/social-marketing/social-handbook-top.png
twitter_image_alt: GitLab's Social Media Handbook branded image
twitter_site: gitlab
twitter_creator: gitlab
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## ⚠️ `THIS HANDBOOK PAGE IS UNDER CONSTRUCTION AND IS NOT CONSIDERED TO BE LIVE ADVICE AT THIS TIME` Please reach out to #social_media_action Slack channel with any questions

## What is Social Advocacy? 

Through a [social media advocacy program](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/team-member-social-advocacy/), we are able to provide GitLab team members fresh news and updates about our organization and as well as throughout our industry. By having a tool in place that provides curated content, you'll be able to build your personal brand, become an expert in your topic, and contribute  to the growth of GitLab.

Curators are selected intentionally by the Social Media team to drive our advocacy content strategy. You are responsible for curating stories to be shared by GitLab team members by adding content to the Bambu platform. Each piece of content added to Bambu should benefit your team or your topic. Please take the training and complete the issue you are assigned to. This will certify you as a curator in this program. 

##### 🔗 [Watch the training here. You'll need to be logged into GitLab's Unfiltered YouTube account in order to access the video.](https://youtu.be/HqeGYKkcovs)


</details>

<details>
 <summary markdown='span'>
 What can I expect out of the training?
 </summary>

This training is condicted by a Strategic Services Consultant and provides a demonstration to help you navigate the platform, and understand the role of a curator. In this training, you will see the platform from a "reader" perspective, and curator's. This will help you learn the Bambu lifecycle. You will then learn how to set up a profile, connect your social media accounts, upload content to the platform, and view metrics. 

</details>

<details>
 <summary markdown='span'>
 This wasn't a live session, where can I ask remaining questions that I have?
 </summary>

Following the training, the social media team will be hosting an AMA so that curators can ask any remaining questions on the platform or the program. Questions and suggestions can also submitted to the #social-advocacy-curators Slack channel. This channel will also help you stay in touch with the curator program and the latest news. This channel is for team members who are identified as content curators only.

If you have questions or comments on the training portion of this program, you can tag @ksundberg directly in your Curator Training issue that has been assigned to you. 

</details>


Current list of curators `tbd this list is pending`

| Name | Topic |
| ------ | ------ | 
| Jennifer Leslie | Awards and contributed articles |
| Jessica Reeder | All Remote |
| Nuritzi Sanchez | Open Source |
| Heather Simpson | Security | 
| Kira Aubrey | Field Marketing / PubSec |
| Madison Taft | Sales | 
| Ash Withers | Brand campaigns | 
| Sara Kassabian | Blogs (may cross into other topics as needed) | 
| cell | cell | 
| cell | cell | 
| cell | cell | 

### Formatting tips for curators 

- Always find and use Twitter handles in the copy suggestions for Twitter. 
- Never use any handles for copy suggestions for LinkedIn or Facebook, as they will not actually tag pages
- Never start a tweet with `@`. If you want to mention a handle first, add a `.`, so it would appear as `.@GitLab`. Without the period, Twitter will believe this is a response and not a broadcast, so the tweet will not appear on people's walls, dramatically reducing reach and making the post ineffective.
- When changing an image or story title, these changes will only appear on LinkedIn. The original title of the story and original image will appear on Facebook and Twitter.
- Regarding hashtags, when in doubt, leave it out. Platforms now focus on trends and topics without them. They can be helpful on LinkedIn, where users can follow a hashtag, but they are not required.
- When using hashtags, replace a word in the copy with a hashtag. Sometimes the tense and other elements don't allow this. Only then should you consider adding hashtags to the end of the copy suggestion. 
   - E.g. #MovingToGitLab: This works when we're using present tense in the copy, `Company x is #MovingToGitLab`. This won't work if we're referencing a move from the past. Where we'd say, `Company x migrated to GitLab and here's what they learned. #MovingToGitLab`
- Always add a CTA to the internal `note`: Consider sharing on social, please share on your channels, etc.
- Photo stories are not a priority and are only shareable on Facebook and Twitter, if we'd ever use these.
