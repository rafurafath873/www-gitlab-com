---
layout: handbook-page-toc
title: "Sarah Daily's README"
description: "Sarah Daily is a Senior Marketing Operations Manager at GitLab."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Sarah Daily's — README.md

I created this personal README document to help you get to know me and how I work. This document is inspired by Tim Hey's README and the Product team README's.

- [GitLab Profile](https://gitlab.com/sdaily)
- [Team Page](https://about.gitlab.com/handbook/marketing/marketing-operations/)
- [LinkedIn](https://www.linkedin.com/in/sarahdaily/)

### About me

- My name is Sarah Daily (last name jokes are welcome and I've also had some people just call me `Daily`). I am a Senior Marketing Operations Manager on the [Marketing Operations team](https://about.gitlab.com/handbook/marketing/marketing-operations/) at GitLab and I focus on content.
- I currently live in Pagosa Springs, CO.
- I was born and raised in northeastern Oklahoma.
- I am an [Advocate (INFJ-A / INFJ-T)](https://www.16personalities.com/infj-personality) personality type.
- I studied media and convergence journalism at Oral Roberts University in Tulsa, Oklahoma. Convergence journlism is just a fancy term to cover media in all channels (online and offline).
- I enjoy gaming in all its form (console, PC, board). I'm active on the `#gaming` Slack channel and I'm a huge fan of retro games and video game music. I have multiple playlists on Spotify solely dedicated to my favorite video game music.

### How I work

- My timezone is MST
- My typical working hours are 7 AM to 4 PM with a 1/2-hour to hour lunch break around 11 AM.
- I follow the Marketing Operations work cadence of 2-week sprints tracked as [Milestones](https://about.gitlab.com/handbook/marketing/marketing-operations/#milestones).

### My typical week

- I reserve Friday for deep work related to current milestone issues. See [Focus Fridays](https://about.gitlab.com/handbook/communication/#focus-fridays).
- I clean up my to-dos and MRs every Friday.
- I use [Clockwise](https://about.gitlab.com/handbook/tools-and-tips/other-apps/#clockwise) to block focus time for me during the week.
- Every morning at 7 AM, I block an hour for personal development and coding. I'm currently using [Codeacademy](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/#growth-and-development-benefit-eligibility) to brush up on JavaScript, Ruby on Rails, Git, and Command Line.

### My typical day

- I get up at 6 AM on most days.
- I start working at 7 AM and take my first break around 11 AM.
- I typically address emails, to-dos, and Slack DMs first.
- I prefer to have meetings in the morning but will meet anytime during my normal working hours.

### What’s keeping me busy

[My open issues](https://gitlab.com/dashboard/issues?assignee_username=sdaily&state=opened)

### Tools in my purview

1. Cookiebot
1. Disqus
1. Figma
1. Frame.io
1. Google Adwords
1. Google Analytics
1. Google Search Console
1. Google Tag Manager
1. Hotjar
1. Keyhole
1. LaunchDarkly
1. Litmus
1. OneTrust
1. PathFactory
1. SEMrush
1. Sitebulb
1. Sprout Social
1. Survey Monkey
1. Swiftype
1. Tweetdeck
1. Typeform
1. Vimeo
1. YouTube

### How to reach me

- **Slack:** Please use the #mktgops channel to contact me and try to [avoid direct messages](https://about.gitlab.com/handbook/communication/#avoid-direct-messages) if possible. I like to focus deeply on the task at hand, so please expect my responses on email and Slack to be asynchronous. Your messages are important to me and I will respond as soon as I can.
- **Coffee chat:** You can schedule a 30 min coffee chat with me via [Calendly](https://calendly.com/sdaily/30min).
- **After hours:** If you have an emergency, you may text me on my cell number provided on my Slack profile. What constitutes an emergency: system or major functionality down from one of the [tools in my purview](#tools-in-my-purview).

### My contributions

- [How I work from anywhere (with good internet)](https://about.gitlab.com/blog/2019/06/25/how-remote-work-at-gitlab-enables-location-independence/)
- [Interview with Sid: Non-Technical Roles to Technical Roles](https://www.youtube.com/watch?v=6_ux_eX4ZYU)
