---
layout: handbook-page-toc
title: "Developer Evangelism Community Response Process"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## How to engage Developer Evangelism in community response

Given the Developer Evangelism team's familiarity with our community and broad knowledge of GitLab, we regularly engage in managing situations that require GitLab to address urgent and important concerns of our community members.

Our team uses the [Community response board](https://gitlab.com/groups/gitlab-com/-/boards/2727876?&label_name[]=Community%20response) to organize tasks.

### Notification

* **Upcoming announcement:** To notify the Developer Evangelism team of an upcoming announcement, product change, or other news event that may elicit a response from our community, the DRI should comment to `@johncoghlan` in a relevant issue or notify the Developer Evangelism team in the [#developer-evangelism Slack channel](https://gitlab.slack.com/archives/CMELFQS4B). 
* **In-progess situation:** To notify the Developer Evangelism team of an urgent situation that is in-progress, please tag the `@dev-evangelism` User Group in a Slack message in a Slack thread or channel where the situation is being discussed or the [#developer-evangelism Slack channel](https://gitlab.slack.com/archives/CMELFQS4B). 
* Please give the Developer Evangelism team as much notice as possible so we can help influence the messaging of the announcement, prepare responses for the community, and schedule coverage. See **Scheduling** below for more details.
* Please add the `Community response` label to related issues, epics and MRs. Our team owns the label for the [gitlab-com](https://gitlab.com/groups/gitlab-com/-/labels?search=community+response) and [gitlab-org](https://gitlab.com/groups/gitlab-org/-/labels?search=community+response) groups. You can use this quick action: `/label ~"dev-evangelism" ~"Community response"` to apply the labels. 

### Preparation 

* In alignment with GitLab's [efficiency value](/handbook/values/#efficiency), the Developer Evangelism will work with the [Directly Responsible Individuals (DRIs)](/handbook/people-group/directly-responsible-individuals/) for the community response situation to help the DRI and other experts directly address the community feedback and questions. 
* When possible, the Developer Evangelism team will typically collaborate with the DRIs on a list of expected questions/concerns from the community and draft responses. When available, we also include materials prepared for media responses, customer responses, or other materials related to the situation in our response preparation. 
* For high-priority announcements and news, we will conduct a practice exercise alongside the DRIs to test our preparation. 
* In many cases, we will want to direct the community to share their feedback in the GitLab Forum. By preparing a post that we can link to from a blog post, announcement, or community response will streamline how we gather feedback feedback and manage responses. This post should be created by a Forum admin in advance as a private post which is published at the time of the announcement. 

### Scheduling

* If a response is expected to require monitoring and responses outside of our normal working hours (typically Monday - Friday, 900 UTC - 2200 UTC), please inform our team with as much notice as possible so we can schedule coverage accordingly. 
* Developer Evangelism team members who cover situations outside of their normal working hours are strongly encouraged to offset the extra time by scheduling time off in the days/weeks before or after the situation. 
* Scheduling should be coordinated with the DRI and tracked in a spreadsheet. Collaboration with the social media team and other teams may be required depending on the amount of coverage required. 
* Developer Evangelists should always be prepared to respond to the release post on release day given the historical performance of release posts on Hacker News. 

### Monitoring 

* Our team will typically monitor the [GitLab Forum](https://forum.gitlab.com/), [Hacker News](/handbook/marketing/community-relations/developer-evangelism/hacker-news/), Reddit, and other online forums.
* We will flag any content that we identify on social media in the appropriate channels, as needed. 
* The goal of monitoring these forums is to identify comments that require a response from GitLab and to respond directly or share those comments with the appropriate team member to respond. 

#### Monitoring automation

We should strive to automate manual tasks for efficiency and to avoid unnecessary stress to individuals in the response team. In many cases, we use [Zapier integrations](/handbook/marketing/community-relations/community-operations/tools/zapier/#current-zaps) to post mentions of GitLab in a Slack channel. 

While we strive for consistency in the process of monitoring the individual community response channels, in some cases it might not be possible due to the nature of the platform (e.g. lack of an API). In cases where automation is not possible,  we distribute monitoring tasks amongst that team.

This is a set of general recommendations for automating the monitoring processes on some of the channels the team will be listening to:

* **Hacker News**:
  * New posts and comments containing the keyword "GitLab" are shared in the [#hn-mentions](https://gitlab.slack.com/archives/CBL93C22D) Slack channel.
  * Posts that reach the front page and contain the keyword "GitLab" are shared in the [#developer-evangelism](https://gitlab.slack.com/archives/CMELFQS4B) Slack channel with additional tags for @dev-evangelism and @sytses 
  * Note: for both of these automations many posts and comments are captured that do not require a response. For example, posts that link to an open source project hosted on GitLab. 
* **GitLab Forum**:
  * [Set up a Zap](/handbook/marketing/community-relations/community-operations/tools/zapier/) that posts on the relevant Slack channel created for internal response coordination. The trigger will be any new post created on the monitored thread in the forum. Depending on the topic's activity, this might mean quite a lot of unfiltered traffic. In this case, the Zap updates could be temporarily turned off at the discretion of the DRI assigned to monitor, and only manually posts that require a response.
  * Optionally, everyone in the response team can subscribe to the specific forum thread to distribute the task of monitoring.
* **Blog Posts**:
  * When there is a forum post linked to from the blog post announcing a change, it is recommended that comments will be disabled to encourage engagement on the forum post and for more efficient monitoring. Check the blog handbook [to disable comments](/handbook/marketing/blog/#comments). 
  * If comments are enabled, new comments will be posted in the [#mentions-of-gitlab](https://gitlab.slack.com/messages/mentions-of-gitlab) Slack channel. Please join that channel for more efficient monitoring.

### Response 

* When possible and appropriate, our team will engage experts to respond to the community. This direct feedback from the DRIs is [appreciated by members of the wider GitLab community](https://news.ycombinator.com/item?id=26261479). It also allows the DRIs and experts to better understand community sentiment and get direct feedback from our community. 
* If necessary, the Developer Evangelists will provide responses to community questions and concerns on the GitLab Forum, Hacker News, and other forums. 
* If a post about GitLab reaches the front page of Hacker News, we may use a Zoom room and Google Doc to collaborate on responses in synchrously. We will link to the Zoom room and Google Doc in the Slack thread or channel where the situation is being discussed or the [#developer-evangelism Slack channel](https://gitlab.slack.com/archives/CMELFQS4B).
* The Developer Evangelism team will rarely engage in responding on Twitter and other social media channels during these announcements and breaking news situations. Twitter, Facebook, and LinkedIn are primarily owned by the Social Media team in these situations.  
* For situations where messaging is sensitive, we will rely on approved messaging from the Corporate Communications team to create our responses. 

### Feedback 

* Retrospectives should be held following all major community response situations to achieve the following: 
  * Assess the quality of our message and prepartion 
  * Assess the level of staffing for the situation, including how much time was invested in covering the event 
  * Identify things we did or could have done to be better prepared which we can apply to future response situations 
  * Update this handbook page and other materials based on what we learned 
* Our team is happy to provide sentiment assessments from our monitoring as part of the reporting or retrospective process for any announcements or in-progress situations which we are asked to support. Please tag us in the appropriate issue or document with a clear ask to request feedback. 
