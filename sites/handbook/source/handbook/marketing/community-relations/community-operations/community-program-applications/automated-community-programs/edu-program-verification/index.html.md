---
layout: handbook-page-toc
title: "GitLab for Education Program Verification"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview
This page contains details regarding the verification process for the GitLab for Education Program. GitLab uses a third party, [SheerId](https://www.sheerid.com/), for verification services. The GitLab for Education Program provides SheerId with the [program requirements](/solutions/education/join/#requirements) and the SheerId platform is able to verify if an individual meets our requirements by matching applicant-provided data against authoritative data sources automatically. SheerID instantly confirms whether or not there is a match and either successfully verifies the applicant or rejects the applicant.

## Forms

SheerID hosts two separate forms for the GitLab for Education Program due to differences in the reference the data set.

### University Teacher Program Form
The University Teacher Program form [insert form link] is the form for all higher education institutions in 194 countries. Details on the country list can be found [here](https://gitlab.com/gitlab-com/marketing/community-relations/community-operations/community-operations/-/issues/46). This form contains the following fields:

- First Name
- Last Name
- Email Address
- Job Title
- Country (drop down list)
- Institution Name (drop down list)

Note: Additional details regarding the verification database limitations can be [found here](https://gitlab.com/gitlab-com/marketing/community-relations/community-operations/community-operations/-/issues/56).

### K-12 Teacher Program Form
The K-12 Teacher Program form [insert form link] is the form for all K-12 institutions in the United States. SheerID does not currently have the reference data for K-12 in other countries, therefore this verification form is limited to the United States.

This form contains the following fields:

- First Name
- Last Name
- Email Address
- Job Title
- Institution Name (drop down list)

GitLab currently does not have the legal and operational requirements in place to offer SaaS licenses to any institution that may have children under the age of 13. Therefore, the K-12 portion of the GitLab for Education Program is on hold until the requirements are in place.
{: .alert .alert-gitlab-orange}


## Verification flow
The SheerId verification flow for the University Teacher Program Form and K-12 Teacher Program Form are detailed here in the [SheerID Flows](https://developer.sheerid.com/concepts#flows) section of the [SheerID Developer Center](https://developer.sheerid.com/).

If applicants are not successful with the automated verification process, they will be prompted to upload a document that shows: full name, school, with a date in the current year. Acceptable examples include TeacherID card with valid date and Pay stub from within the last 60 days.

Applicants will have 3 attempts to upload a document providing verification of eligibility. SheerID will manually review these documents.  Upon the third unsuccessful attempt, applicant will be rejected.

If SheerID is unable to determine the eligibility based on the document review, SheerID will forward the applicant's record to education@gitlab.com. The GitLab for Education team will review the documentation and determine eligibility.

## Communications

The browser notifications and emails sent by SheerID can be found here [ TBD].

## Verification limit

Any individual can apply through the form and verify up to 5 times per 365 days. This limit provides room for error and allows a single individual at an institution to apply for licenses on behalf of individual sub-units within the institution. For example, and information technology administrator may apply and hold the license for multiple colleges or departments on one campus.

This limit is determined by GitLab and set in the system by SheerID. It can be changed at any time. 
