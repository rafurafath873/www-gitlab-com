---
layout: handbook-page-toc
title: "IR.2.03 - Incident External Communication Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# IR.2.03 - Incident External Communication

## Control Statement

GitLab communicates a response to external stakeholders as required by the Incident Response Plan.

## Context

This control demonstrates that we can provide evidence of communication in the event of an incident to external stakeholders.

## Scope

This control applies to the external communication of security incidents.

## Ownership

* Control Owner: `Security Operations`
* Process owner(s):
    * Security Operations: `50%`
    * Security Communications: `50%`

## Guidance

The spirit of this control is to ensure that external communication of security incidents is conducted in accordance with GitLab's established security communications plan. This ensures all appropriate stakeholders are notified and engaged as appropriate.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [control issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/issues/844).

Examples of evidence an auditor might request to satisfy this control:

* Provide GitLab's incident external communication plan
* Provide samples showing the plan is followed for Infrastructure incidents
* Provide samples showing the plan is followed for Security incidents

### Policy Reference

* [Security Incident Communication Plan](/handbook/engineering/security/security-operations/sirt/security-incident-communication-plan.html)
* [Incident Management - Communication](/handbook/engineering/infrastructure/incident-management/#communication)

## Framework Mapping

* PCI
  * 12.10.1
