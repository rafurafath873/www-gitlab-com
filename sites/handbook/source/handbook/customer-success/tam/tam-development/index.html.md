---
layout: handbook-page-toc
title: "TAM Development"
description: "Available development paths and resources for Technical Account Managers at GitLab."
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

As a Technical Account Manager, it is essential to understand what resources are available for your personal and professional development.   This page strives to be a place for leadership and TAMs to contribute to resources available for the progression and development of the TAM team.

## Key handbook sections, roadmaps and L&D platforms for enablement:
1. [TAM Planned Enablement Roadmap for H1 FY22](https://docs.google.com/presentation/d/1HCoPkdjucC7nUaJl5eNzj3VNCWFdQ3nWvUNiMv2tcDU/edit#slide=id.g605d07d6c5_2_96)
1. [Customer Success Education & Enablement (All-CS)](/handbook/customer-success/education-enablement/)
1. [New TAM Sales Quickstart Learning Path](https://about.gitlab.com/handbook/sales/onboarding/sales-learning-path/): Delivered through scheduled training sessions and pre/post work, with portions applicable only to the TAM role.
1. [Edcast](https://gitlab.edcast.com/): For certifications, learning paths and more!


## Personal development

### General skills

- [GitLab's Learning Experience Platform](https://about.gitlab.com/handbook/people-group/learning-and-development/gitlab-learn/), [GitLab Learn](https://gitlab.edcast.com/?fromLogin=true). We are building courses to upskill, reskill, and grow team member skills. If you have ideas about a course you'd like to develop, consider checking out the [Learning Evangelist Learning Path](https://gitlab.edcast.com/pathways/learning-evangelist-training).

#### LinkedIn Learning

[Handbook details about LinkedIn Learning](/handbook/people-group/learning-and-development/linkedin-learning/).

Recommended courses:

- [The Six Morning Habits of High Performers](https://www.linkedin.com/learning/the-six-morning-habits-of-high-performers/six-practices-to-get-back-on-track)
- [Unconscious Bias](https://www.linkedin.com/learning/unconscious-bias/)
- [Devops Foundations](https://www.linkedin.com/learning/devops-foundations/)
- [How to Make Strategic Thinking a Habit](https://www.linkedin.com/learning/how-to-make-strategic-thinking-a-habit/why-make-strategic-thinking-a-habit?u=2255073)
- [Psychological Safety: Clear Blocks to Innovation, Collaboration, and Risk-Taking](https://www.linkedin.com/learning/psychological-safety-clear-blocks-to-innovation-collaboration-and-risk-taking/psychological-safety-clear-blocks-to-problem-solving-and-innovation?u=2255073)
- [Improving Your Listening Skills](https://www.linkedin.com/learning/improving-your-listening-skills/welcome?u=2255073)

We also have a list of [Recommended Learning Paths](https://about.gitlab.com/handbook/people-group/learning-and-development/linkedin-learning/#recommended-learning-paths).

### Soft skills
- [Developing your Emotional Intelligence](https://www.linkedin.com/learning/developing-your-emotional-intelligence/benefits-of-building-emotional-intelligence)
- [Leading with Emotional Intellignece](https://www.linkedin.com/learning/leading-with-emotional-intelligence-3/lead-with-emotional-intelligence)

### Technical

- Self Certifications are available for the professional services courses on Edcast:
  * [CI/CD Specialist Pathway](https://gitlab.edcast.com/pathways/copy-of-gitlab-certified-ci-cd-specialist-pathway-what-s)
  * [GitLab Certified Associate](https://gitlab.edcast.com/pathways/copy-of-gitlab-certified-associate-pathway)
- [GitLab Technical Training delivered by Professional Services](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/gitlab-technical-certifications/)
- MIT's online program: [Cloud & DevOps: Continuous Transformation](https://professionalprograms.mit.edu/es/programa-online-cloud-devops-transformacion-continua/) covers strategical implementation of the DevOps and Cloud tools
- [Cloud Infrastructure Technical Skills](https://about.gitlab.com/handbook/customer-success/education-enablement/#cloud-infrastructure) 

### Leadership

- L&D is running another org-wide [Manager Challenge](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-challenge/) program in early May, 2021-05-03 to 2021-05-14. Open to existing *and* aspiring managers! [Sign Up Here](https://gitlab.com/gitlab-com/people-group/learning-development/challenges/-/issues/55)
- We are currently revamping our mentorship program and [manager lean coffees](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/123) as a first step
- We have two team members in L&D who will be certified in [Crucial Conversations](https://about.gitlab.com/handbook/leadership/crucial-conversations/) at the end of Q1 FY22. We will start rolling this out to the organization in Q2/Q3, perhaps that is something the TAM team can take advantage of. More to details to come!
- [CEO Shadow Program](/handbook/ceo/shadow/)
- [Sounding Board](https://www.soundingboardinc.com/request-demo/): External resource with online coaching
- [New Manager Foundations](https://www.linkedin.com/learning/new-manager-foundations-2/)
- [Being a good mentor](https://www.linkedin.com/learning/being-a-good-mentor/)

## Career development

### Within the TAM job family
- The [TAM Job Family page](https://about.gitlab.com/job-families/sales/technical-account-manager/) is kept updated with the roles and responsibilities of each level within the TAM job family
- The [TAM Skills Matrix](https://docs.google.com/spreadsheets/d/1_UEke64Qkz8wSyqfr_E9qqeAF6rX77w4vIH84Ckm_ts/edit#gid=0) enables a TAM to determine areas of opportunity in working towards their next level of seniority


### Within the CS org
- [The SA Job Family page](https://about.gitlab.com/job-families/sales/solutions-architect/) is kept update with the roles, responsibilities and technical expectations of an SA. For TAMs interested in moving into SA roles, they can work with their managers on learning paths to develop the technical and sales skills where needed.

### Within GitLab, outside of CS
- While there is no 'formula' for moving into roles in other teams, we welcome TAM growth and desire to explore alternative areas with GitLab.  For TAMs looking to move into different groups outside of CS, the first step is to review the job family requirements and connect with their manager on determining a path. It is encouraged for managers to connect with the group's team leaders in question to build out a development plan that will have the best chances of success. 
