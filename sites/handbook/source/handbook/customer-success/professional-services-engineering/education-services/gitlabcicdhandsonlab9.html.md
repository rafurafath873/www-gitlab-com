---
layout: handbook-page-toc
title: "GitLab CI/CD Hands On Guide- Lab 9"
description: "This Hands On Guide Lab is designed to walk you through the lab exercises used in the GitLab CI/CD training course."
---
# GitLab CI/CD Hands On Guide- Lab 9
{:.no_toc}

## LAB 9- SECURITY SCANNING

1. In the GitLab Demo Cloud, locate your CICD Demo project from previous labs and open it.
2. In a seperate tab, open the **ci-sast snippet** and click the **Copy file contents** icon. 
3. Return to your CICD Demo project opened in your other tab and open the gitlab-ci.yml file. 
4. Click the **Edit** icon and **paste the contents of the snippet at the end of the file.**
5. In the Commit Message field, enter  “**enable sast**” -> **Commit to Master Branch**. 
6. Navigate to your Pipeline and click on the **gosec-sast** job to ensure it is running. 
7. To view the results of your scan, on the left-hand pane, click **Security & Compliance > Vunlerabiltiy Report**. 
8. In the Scanner drop-down list, click on **SAST** to view the results of your most recent scan. 

### SUGGESTIONS?

If you wish to make a change to our Hands on Guide for GitLab CI/CD- please submit your changes via Merge Request!
