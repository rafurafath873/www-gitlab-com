---
layout: markdown_page
title: "Women at GitLab Mentorship Program"
description: "Mentorship opportunities for women at GitLab sponspored by a partnership between the Women TMRG, GitLab Learning and Development team, and the GitLab DIB team"
canonical_path: "/company/culture/inclusion/tmrg-gitlab-women/mentorship-program"
---

## Women at GitLab Mentorship Program 

The Women at GitLab Mentorship program is a career development opportunity for participants to build trust across the organization, enable pathways for internal growth and development, and increase collaboration across teams. The program is hosted in collaboration with the Women's TMRG, GitLab Learning and Development team, and the GitLab DIB team.

The Women at GitLab Mentorship Program will begin in 2021-07. Applications for both mentors and mentees will be colleted in 2021-05 and 2021-06. The program will run for 3 months, from 2021-07 to 2021-09. Please see the timeline section on this page for further details.

### History

In 2020, the Women's TMRG partnered with the Sales Organization at GitLab to offer the [Women in Sales Mentorship Program pilot](https://about.gitlab.com/handbook/people-group/women-in-sales-mentorship-pilot-program/#women-in-sales-mentorship-program-pilot). This initial iteration was a huge success and inspired the scale of this iteration to include a wider audeince. 

Some of the program structure for this iteration has been adapted to enable greater participation and fit the capacity of the L&D team. The mission, benefits, and goals of the program will stay the same.

### Structure

**Initial program kickoff**

All mentors and mentees will gather for a live program kickoff to recap program goals, answer questions, address concerns, and network. We'll host multiple sessions of this kickoff to accommodate workings hours of all participants. The session will run between 30 and 45 minutes.

**1:1 Mentor Sessions**

The program proposes a mentor/mentee relationship between selected mentee applicants and mentors across the company. Sessions will take place every other week for 30-minutes and will last for up to 3 months (with the possibility of an extension), as long as both mentors and mentees remain engaged. The mentor/mentee relationship will be cross-divisional, meaning that both parties will have the opportunity to work with and learn from team members outside of their respective divisions.

**Mentor/Mentee Workbook**

Async resources will be provided as a training guide for all mentor/mentee pairs. This workbook will include suggested articles to read and discuss, strateiges for goal setting, sample meeting agendas, and additional training material. While it's not required that these workbooks guide the relationship, they will be a useful resource when mentors or mentees feel stuck or need a starting place for discussion.

**Mentor/Mentee Training**

The program will include 1 live, 50 minute training session (the option to watch the recording will be available for those who cannot attend) to review best practices for both mentors and mentees. It's important that participants make every effort to attend this session to ensure the greatest results from the program and relationship.

**Mid-Program Panel AMA**

At the program half-way point, we'll host multiple AMA sessions to check in on the current program status. The AMA will be planned for a 50 minute meeting. This session will be composed of two parts:

1. A panel AMA with mentors and mentees to discuss common questions and conerns
1. Small group discussions to share best pracitces and biggest takeways so far

**End of Program Celebration**

To conclude the program, mentors and mentees will gather for small and full group discussions to share biggest takeaways from the program. We'll host multiple sessions of this celebration to accommodate workings hours of all participants. The celebration call will be 50 minutes long.


### Timeline

| Date | Description | Related Resources |
| ----- | ----- | ----- |
| 2021-05-05 | Discuss program details with Women's TMRG group meeting | [Planning Issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/122) |
| 2021-05-18 - 2021-05-28 | Call for Mentors | Application forthcoming |
| 2021-05-31 - 2021-06-11 | Call for Mentees | Application forthcoming |
| 2021-06-18 | Pairing of mentor relationships complete and communicated to applicants |
| Between 2021-06-18 and 2021-07-01 | Optional: Mentor/Mentee pre-program coffee chats | |
| Between 2021-07-01 and 2021-07-07 | Initial program Kickoff meeting | Date TBD |
| Week of 2021-07-12 | Training for Mentors and Mentees | Date and Resources TBD |
| Week of 2021-08-16 | Mid program AMA | Date TBD |
| Week of 2021-09-20 | End of program celebration | Date TBD |


### Mentoring resources

Existing [resources for mentors](https://about.gitlab.com/handbook/people-group/learning-and-development/mentor/) are available in the L&D handbook. As part of this program, a detailed mentor/mentee relationship workbook will be created in the handbook and used to guide mentoship sessions and communications. Please stay tuned for these resources as the program kick off gets closer.

### Being a Mentor

#### Benefits of being a mentor

As a mentor, you beneift from:

- Fast feedback loop between team members and leadership
- Opportunity to form relationships with team members in other departments
- Opportunity to support Women at GitLab and live our values

#### Mentor requirements

- You have been with GitLab for at least 3 months at the time of application 
- You have availability in your schedule to meet with your mentee on a bi-weekly basis for at least 30 minute meetings for the entire length of the program
- You plan to make every effort to attend all live trainings included in the program structure
- You are not on a PDP (Performance Development Plan) or PIP (Performance Improvement Plan)
- You can complete the [DIB training certification](https://gitlab.edcast.com/journey/ECL-5c978980-c9f0-4479-bf44-45d44fc56d05) before the program begins


#### Apply to be a Mentor

Please complete [this Mentor Application Google form](https://docs.google.com/forms/d/e/1FAIpQLSfkoP5XXB4Zz_wVqbEvrMlATmp710A9CDRM6GegXRIxSX4KqQ/viewform) to apply to be a mentor.

Please note that applying for a mentor does not guarentee you a spot in the program, and eligibility will be evaluated based on criteria above.

If you've participated in a previous GitLab mentor program, you are welcome to apply again!

#### Considerations

Applications will be prioritized on many points including:

- Skill and prior experience as a mentor
- Performance in current role

### Being a Mentee

#### Benefits of being a mentee

As a mentee, you benefit from:

- Increased visibility with leadership
- Increased professional development opportunities
- Career coaching and guidance
- Opportunity to form relationships with leaders on other teams

#### Mentee requirements

- You're a woman-identifying team member at GitLab. Gender non-conforming folks who face workplace challenges similar to those of women-identifying team members are also welcome to apply.
- You have been with GitLab for at least 3 months at the time of application 
- You have availability in your schedule to meet with your mentor on a bi-weekly basis for at least 30 minute meetings for the entire length of the program
- You plan to make every effort to attend all live trainings included in the program structure
- You are not on a PDP (Performance Development Plan) or PIP (Performance Improvement Plan)

#### Considerations

Applications will be prioritized on many points including:

- Team members on a management career track
- Performance in current role

#### Apply to be a Mentee

Application for mentees will open on 2021-05-31.

Please note that applying for a mentee does not guarentee you a spot in the program, and eligibility will be evaluated based on criteria above.


### Metrics and Outcomes

Mentors and mentees will complete a mid-program and end of program survey to collect feedback, assess skills, and determine if goals set during mentor/mentee relationships have been set.

Success of this program will be measured by:

1. percentage of mentor/mentee pairs who meet consistently on a bi-weekly basis, with a goal of 95% completion
1. attendance to live training sessions, with a goal of 95% attendance
1. engagement during live training sessions
1. overall feedback of the program from both the mentor and mentee perspective


### Common Questions

**Can I participate as both a mentor and a mentee?**

Yes, this is a possibility. It's important that team members talk with their managers to discuss their capacity to participate in both roles in this program. Participation in both roles will require a 2hr commitment per month (two 30 minute sessions as a mentor and two 30 minute sessions as a mentee).

It's possible that we will have a shortage of mentors and be unable to complete all mentor/mentee pairs. If there is a shortage of mentors and you've applied as both a mentor and a mentee, you will be assigned **only** as a mentor in this program. 

**I'd like to particiapte but I can't commit the entire length of the program. Can I still apply?**

If you cannot commit to the 3 month program, please consider applying for a future mentorship program at GitLab. It's important that both mentors and mentees are available for the full 3 month program to acheive the best results.

**I'm not elidigible to be a mentor or a mentee in this program. What are my options for future participation?**

The L&D team is working with the DIB team to create a mentor program framework that can be implemented for teams across GitLab. Please watch for updates on this iteration in FY22-Q3.

In addition, team members are always encouraged to build mentor/mentee relationships independently. You can find resources and a list of available mentors in the [Learning and Development handbook](/handbook/people-group/learning-and-development/mentor/)


### Additional questions?

Questions, comments, and feedback can be shared in this [issue](https://gitlab.com/gitlab-com/women-tmrg/-/issues/31). For additional questions, please reach out to `@slee24` in the [#women](https://app.slack.com/client/T02592416/C7V402V8X) or [#learninganddevelopment](https://app.slack.com/client/T02592416/CMRAWQ97W) Slack channels with questions or concerns.
