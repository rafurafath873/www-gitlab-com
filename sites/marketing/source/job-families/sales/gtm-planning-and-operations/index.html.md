---
layout: job_family_page
title: "Go-to-Market Planning and Operations"
description: "The Go-to-Market (GTM) Planning and Operations job family supports GitLab's go-to-market planning and compensation design, as well as the associated analytics and tools."
---

The Go-to-Market (GTM) Planning and Operations job family supports GitLab's go-to-market planning and compensation design, as well as the associated analytics and tools. They partner cross functionally across the Chief Revenue Officer's organization.

## Responsibilities

* Iterate on the Field Planning & Bottoms-up Quota Capacity Models; maintain throughout the year and use to drive insights that improve efficiency of the GTM motion
* Drive all aspects of compensation design for roles across the CRO organization (including core Sales, Customer Success, Channel, Alliances, Management and others); Collaborate closely with Sales, Finance, Sales Commissions & Ops in the process
* Deliver insightful comp analytics that lead to improvements in the design and effectiveness of compensation models 
* Support quota/compensation troubleshooting throughout the year 
* Support the evaluation and implementation of software tools required for Field, Quota and Territory planning

## Requirements

* Ability to partner, collaborate and influence across functional areas (e.g. Finance, People Operations and Sales) and support multiple business partners
* Excellent problem solving, project management, interpersonal and organizational skills
* Previous experience in Consulting, Banking, PE or Strategy roles a big plus 
* Basic SQL skills a big plus; we have experts, but it helps tremendously 
* SFDC and Xactly expertise and knowledge of typical enterprise SaaS tools 
* SaaS and B2B experience preferred
* Interest in GitLab, and open source software
* Share our [values](/handbook/values/), and work in accordance with those values.
* Ability to use GitLab

## Levels

### Senior Manager, GTM Planning and Operations

The Senior Manager, GTM Planning and Operations reports to the [Senior Director, Sales Strategy](/job-families/sales/sales-strategy/).

#### Senior Manager, GTM Planning and Operations Job Grade

The Senior Manager, GTM Planning and Operations is a [9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Manager, GTM Planning and Operations Responsibilities

* Be a key driver and architect of the annual Go-To-Market planning process for the CRO organization
* Partner closely with Sales Leadership, Finance and Sales Operations to help design an efficient, effective and predictable GTM motion
* Be a key leader in the design and implementation of Land vs. Expand experiments 

#### Senior Manager, GTM Planning and Operations Requirements

* BA/BS degree in engineering, accounting, finance, economics or other quantitative fields preferred
* 8+ years relevant experience and a solid understanding of go-to-market design & sales planning as well as sales compensation design & analytics 
* Advanced analytical and financial modeling skills with a superb ability to drive high value insights 
* Meet the [Leadership at Gitlab](/company/team/structure/#management-group) requirements

### Director, GTM Planning and Operations

The Director, GTM Planning and Operations reports to the [Sr. Director, Sales Strategy](/job-families/sales/sales-strategy/).

#### Director, GTM Planning and Operations Job Grade

The Director, GTM Planning and Operations is a [10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director, GTM Planning and Operations Responsibilities

* Extends the Senior Manager, GTM Planning and Operations Responsibilities
* Design and drive the annual Go-To-Market planning process for the CRO organization;
* Own on the Field Planning & Bottoms-up Quota Capacity Models; maintain throughout the year and use to drive insights that improve efficiency of the GTM motion
* Responsible for the design and implementation of Land vs. Expand experiments 
* Deliver structured, actionable and insightful comp analytics that lead to improvements in the design and effectiveness of compensation models 
* Be a thought leader in quota/compensation troubleshooting throughout the year 
* Drive the evaluation and implementation of software tools required for Field, Quota and Territory planning
* Meet the [Leadership at Gitlab](/company/team/structure/#management-group) requirements

#### Director, GTM Planning and Operations Requirements

* Extends the Senior Manager, GTM Planning and Operations Requirements
* 10+ years relevant experience and a solid understanding of go-to-market design & sales planning as well as sales compensation design & analytics 

## Performance Indicators

* [IACV vs. plan > 1](/handbook/sales/#incremental-annual-contract-value-iacv)
* [IACV efficiency > 1.0](/handbook/sales/#iavc-efficiency-ratio)
* [Win rate > 30%](/handbook/sales/#win-rate)
* [Rep IACV per comp > 5](/handbook/sales/#measuring-sales-rep-productivity)

## Career Ladder 

The next steps in the GTM Planning and Operations job family is not yet defined at GitLab.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

- Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters,
- Next, candidates will be invited to schedule a 25 minute interview with the Hiring Manager,
- Next, candidates can expect 2-5 separate 25 minute interviews with other Team Members, 
- Finally, candidates will be invited to schedule a 50 minute interview with an Executive. 

Additional details about our process can be found on our [hiring page](/handbook/hiring).
