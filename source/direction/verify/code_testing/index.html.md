---
layout: markdown_page
title: "Category Direction - Code Testing and Coverage"
description: "The GitLab Code Testing and Coverage direction page."
canonical_path: "/direction/verify/code_testing/"
---

- TOC
{:toc}

## Code Testing

Code testing and coverage ensure that individual components built within a pipeline perform as expected. This is a core piece of the Ops Section [direction](/direction/ops/#smart-feedback-loop) "Smart Feedback Loop" between developers and we are aiming to make that as [reliably speedy](/direction/ops/#speedy-reliable-pipelines) as possible, eventually enabling users to go from first commit to code in production in only an hour with confidence.

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ACode%20Testing%20and%20Coverage)
- [Overall Vision](/direction/ops/#verify)

Interested in joining the conversation for this category? Please join us in the issues where we discuss this topic and can answer any questions you may have. Your contributions are more than welcome.

This page is maintained by the Product Manager for Testing, James Heimbuck ([E-mail](mailto:jheimbuck@gitlab.com))

## What's Next & Why
We hear from users that they want to enforce that test coverage within a project [should not be allowed](https://gitlab.com/gitlab-org/gitlab/-/issues/15765) to decrease with changes. It is possible to do this with some scripts and fail the pipeline but it is not a great user experience and is easy to circumvent.

This is why we are next starting on a feature that [enforces test coverage must](https://gitlab.com/groups/gitlab-org/-/epics/4366) stay or increase beyond the current level with a merge request before it can be merged. 

## Who we are focusing on? 

Check out our [Ops Section Direction "Who's is it for?"](/direction/ops/#who-is-it-for) for an in-depth look at our target personas across Ops. For Code Testing and Coverage, our "What's Next & Why" are targeting the following personas, as ranked by priority for support: 
1. [Delaney - Development Team Lead](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)
1. [Sasha - Software Developer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
1. [Simone - Software Engineer in Test](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#simone-software-engineer-in-test)
1. [Devon - DevOps Engineer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)

## Maturity Plan

This category is currently at the "Viable" maturity level, and our next maturity target is "Complete" (see our [definitions of maturity levels](/direction/maturity/)). Key deliverables to achieve this are included in these epics:

* ~~[Code Coverage Data for groups](https://gitlab.com/groups/gitlab-org/-/epics/2838)~~ Delivered in 13.9
* ~~[Test History for MRs and Pipelines](https://gitlab.com/groups/gitlab-org/-/epics/4155)~~ Delivered in 13.9
* [Historic Test Data for projects](https://gitlab.com/groups/gitlab-org/-/epics/3129)

We may find in research that only some of the issues in these epics are needed to move the vision for this category maturity forward. The work to move the maturity is captured and being tracked in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/3660).

## Competitive Landscape

In the [2021 Continuous Softward Delivery Forrester Tech Tide](https://www.forrester.com/report/The+Forrester+Tech+Tide+Continuous+Software+Delivery+Q1+2021/-/E-RES161669), Testing was cited as the number one key to unlock continuous delivery for organizations. Top areas for investment are a) API test automation, b) continuous functional test suites, c) shift-left performance testing. Industry leaders are seeking integrated suites over best in breed tools for testing and CD. Additionally, API testing is being marketed as a silver bullet that is cheaper, effective and efficient to modernize the toolchain for enterprises. Sample vendors include: API Fortress, Broadcom, Eggplan, and  others. We are exploring how we expand our market share in this area via [product#2516](https://gitlab.com/gitlab-com/Product/-/issues/2516) and adding a new category in this [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/80183). 

Many other CI solutions can also consume standard JUnit test output or other formats to display insights natively like [CircleCI](https://circleci.com/docs/2.0/collect-test-data/) or through a plugin like [Jenkins](https://plugins.jenkins.io/junit). 

There are new entries in the code testing space utilizing ML/AI tech to optimize test execution like [Launchable](https://launchableinc.com/solution/) and even write test cases like [Diffblue](https://www.diffblue.com/).

In order to stay remain ahead of these competitors we will continue to push forward to make unit test data visible and actionable in the context of the Merge Request for developers with [unit test reports](https://docs.gitlab.com/ee/ci/unit_test_reports.html#viewing-unit-test-reports-on-gitlab) and historical insights to identify flaky tests with issues like [gitlab#33932](https://gitlab.com/gitlab-org/gitlab/issues/33932).

## Top Customer Success/Sales Issue(s)

Sales has requested a higher level view of testing and coverage data for both projects and groups from the Testing Group. Our first step towards this was the display of [coverage data for groups](https://gitlab.com/groups/gitlab-org/-/epics/2838) the first iteration of which has shipped. We believe that Test Coverage and [Test Execution data](https://gitlab.com/gitlab-org/gitlab/-/issues/322018) are signals that will help Team Leads guide their teams to a higher quality project.

We believe a good first step to helping customers engage with the coverage data for groups and realize value will be to [show some projects](https://gitlab.com/gitlab-org/gitlab/-/issues/323666) with recent coverage data by default on the report and display the [difference in coverage from 30 days prior](https://gitlab.com/gitlab-org/gitlab/-/issues/322661) so it is easier to identify which projects are trending down and need attention.

## Top Customer Issue(s)

The most popular customer issue is a request to support [JaCoCo coverage reports](https://gitlab.com/gitlab-org/gitlab/-/issues/227345) for the [test coverage visualization feature](https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html). While there is a documented way to transform JaCoCo reports to the supported Cobertura format by running an extra job to do this is contrary to our vision of [Speedy, Reliable Pipelines](https://about.gitlab.com/direction/ops/#speedy-reliable-pipelines) and our product principle of [working by default](https://about.gitlab.com/handbook/product/product-principles/#configuration-principles). 

## Top Internal Customer Issue(s)

The GitLab Quality team is interested in being able to [detect and report on flaky tests](https://gitlab.com/gitlab-org/gitlab/-/issues/3673). The MVC to count test failures was a good start and they are analyzing data from that feature to look at ways to improve.

The GitLab Quality team also opened an interesting issue, [Provide API to retrieve test case durations from a pipeline](https://gitlab.com/gitlab-org/gitlab/issues/14954), that is aimed at solving a problem where they have limited visibility into long test run times that can impact efficiency.

## Top Analyst Landscape Items

In 2020, Gartner has released the Artificial Intelligence Use Case Prism for Development and Testing on their [research website](https://www.gartner.com/en/documents/3994888/infographic-artificial-intelligence-use-case-prism-for-d). Directionally, several of the use cases are generation of unit tests from analyzing code patterns, using business logic to create API test scenarios, and using machine learning to fabricate test data as well as correlating testing results back to business metrics to convey meaningful connections like release success or quality. 

## Top Vision Item(s)
To realize our long term vision we need to add more value not just for users uploading junit.xml and Cobertura reports but for any users with test and coverage reports. We believe that the best way to do this is to make it easy for users to contribute additional parsers so they can access the features the team is building that use the data. This will allow wider community contributions and is in alignment with [GitLab's Dual Flywheel strategy](https://about.gitlab.com/company/strategy/#dual-flywheels). A first step towards this could be a [GitLab-specific unit test report](https://gitlab.com/gitlab-org/gitlab/-/issues/247975).

We are also looking to provide a one stop place for CI/CD leaders with [Director-level CI/CD dashboards](https://gitlab.com/gitlab-org/gitlab/issues/199739). Quality is an important driver for improving our users ability to confidently track deployments with GitLab and so we are working next on a view of test execution data over time within [projects](https://gitlab.com/groups/gitlab-org/-/epics/3129).

We have started brainstorming some ideas for the vision and captured that as a rough design idea you can see below.

![Design for Vision of Code Testing and Coverage data summary](/images/code-testing-data-view-vision.png)

