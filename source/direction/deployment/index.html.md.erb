---
layout: markdown_page
title: "Deployment Direction"
description: "The job of deploying software is a critical step in DevOps. This page highlights GitLab's direction."
canonical_path: "/direction/deployment/"
---

- TOC
{:toc}

<%= devops_diagram(["Configure","Release"]) %>

## Overview

### What is Deployment?

There is nothing more valuable than user feedback in production. Deployment is the step which brings coded, integrated, and built software to production environments so you can start receiving that feedback. Deployment is part of GitLab's [Ops](/direction/ops/) section.

Deployments are increasing in form and freqency. 
* Deployment **frequency** is increasing because of competitive pressures for organizations to tighten user feedback loops. Adoption of Agile and DevOps practices and culture enable firms to deploy every minute. 
* Deployment **form** is changing because of the adoption of cloud infrastructure, container orchestration and GitOps. Infrastructure as Code and GitOps enables new infrastructure for applications to be deployed in minutes and new application changes to be seamlessly and progressively pulled into existing infrastructure.

One side effect of this evolution is that the line between the configuration of infrastructure and the release of software has been blurred. In order to clearly communicate our vision and strategy for these stages, GitLab's deployment direction is inclusive of both the [Configure](/direction/configure/) stage and the [Release](/direction/release/) stage to account for the advancement in deployment practices. 

### Deployment Vision

GitLab's Deployment vision is to enable deployment to be fast and easy, yet flexible enough to support the scale and operating model for your business.

We want you to spend the majority of your time creating new software experiences for your users instead of investing time and effort on figuring out how to get it into their hands. No matter if you are operating your own infrastructure or are cloud-native, GitLab is your one stop-deployment-shop from AutoDevOps to building your organization's own platform-as-a-service.

### Market
The total addressable market (TAMkt) for DevOps tools targeting the Release and Configure stages was [$1.79B in 2020 and is expected to grow to $3.25B by 2024 (13.8% CAGR) (i)](https://docs.google.com/spreadsheets/d/1LO57cmXHDjE4QRf6NscRilpYA1mlXHeLFwM_tWlzcwI/edit?ts=5ddb7489#gid=1474156035). This analysis is considered conservative as it focuses only on developers and doesn't include other users. External market sizing shows a much bigger potential. For example, for continous delivery alone, which does not include categories such as infrastructure as code, is estimated to have a [market size of $1.62B in 2018 growing to $6B by 2026 (17.76% CAGR)](https://www.verifiedmarketresearch.com/product/continuous-delivery-market/). This, like the [Ops market](/direction/ops/#market) in general, is a deep value pool and represents a significant portion of GitLab's expanding addressable market. 

The deployment tooling market is evolving and expanding. There are now many more options than just adding a delivery job to your CI/CD pipeline. Advanced deployments, GitOps, infrastructure provisioning, platform as a service, and feature flags are all methodologies that help teams deploy more easily, frequently, and confidently. Completeness of feature set from vendors is becoming increasingly important as teams want the benefits from all worlds; traditional, table stakes deployment features alongside modern, differentiating features.

Enterprises, to increase deployment frequency and be competitive in market, have turned to [centralized cloud teams or cloud center of excellence](https://gitlab.com/gitlab-com/Product/-/issues/2287#note_526578502) that are responsible for helping [development teams be more efficient and productive](https://gitlab.com/gitlab-com/Product/-/issues/2287#note_526579050). These teams have centralized buying power for DevOps and deployment tools. They may also have organizational support to build a DIY DevOps platform. For DIY platforms, open-source point deployment solutions (such as [Flux](https://fluxcd.io/) or [ArgoCD](https://argoproj.github.io/argo-cd/)) often become the primary option for deployments.
  
### Current Position
The CI/CD pipeline is one of GitLab's most widely used features. Many organizations that have adopted the GitLab pipeline also use GitLab for deployment because it is the natural continuation of their DevOps journey.

Using the CI/CD pipeline, users can create their deployment jobs by writing their own custom `.gitlab-ci.yml` file or using a pre-defined [GitLab Template](https://docs.gitlab.com/ee/ci/examples/README.html#cicd-templates). Users can also leverage GitLab provided containers to solve for specific deployment scenarios, such as [deploying to AWS](https://about.gitlab.com/blog/2020/12/15/deploy-aws/). 

Beyond using the generic pipeline, GitLab offers [AutoDevOps](https://docs.gitlab.com/ee/topics/autodevops/) that enables users to get up and running quickly and easily yet maintain control of each step of the DevOps process.

There are also additional ways to deploy using GitLab. For example, you can [deploy to an attached cluster](https://docs.gitlab.com/ee/user/project/clusters/#deploying-to-a-kubernetes-cluster). You can now also run pull-based GitOps using the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/). Lastly, we are working to make it easier to quickly deploy a stateful app that runs on hypercloud via experimenting through the [5-minute production app](https://gitlab.com/gitlab-org/5-minute-production-app/deploy-template/-/tree/master).

This represents at least 7 deployment-related options within GitLab.

1. Custom `.gitlab-ci.yml`
1. Templated `.gitlab-ci.yml`
1. Cloud specific deployment tools
1. Auto DevOps
1. GitLab attached Kubernetes cluster
1. Kubernetes Agent
1. 5-minute Production app

Given the myriad of options that exist, we intend to make clearer what is the right solution for the job and potentially consolidate the choices and lean into our [convention over configuration](https://about.gitlab.com/handbook/product/product-principles/#convention-over-configuration) product principle.

Despite the deployment options GitLab provides, we also acknowledge that there are instances where users will choose to integrate with another tool for deployment. If that is the case, we'd love to hear from you to understand why.

### Challenges

The [Ops section challenges](/direction/ops/#challenges) are applicable to deployment. There are some challenges that are worth expanding on with some additional highlights:

* CNCF/cloud-native open-source solutions can disrupt GitLab's one application vision. They are marketed to a huge and engaged audience. If they're successful in growing adoption, it introduces a barrier to adopting GitLab. For deployment, a risky and highly visible part of the SDLC process, organizations may be more reticent to switch to GitLab's one application solution.
* Our target customers are also our main competition as cloud platform teams often have a charter, or at least leeway, to build a DIY platform.
* GitLab's pipeline-based deployment solution targets the developer as the primary persona. As a deployment tool, it may be less effective relative to solutions that target the operator as the primary persona with specific tooling made for their primary jobs.

### Opportunities
The [Ops section opportunities](/direction/ops/#opportunities) apply to deployment. GitLab also has some important tail-winds specific to deployment:

* The CI/CD pipeline is still central to most Software Development Life Cycle's workflow. A large portion of GitLab customers would like using a pre-integrated deployment solution offered by GitLab that fits their need. 
* Cross-Use Case Workflows: As a single DevOps platform we have a unique opportunity to connect multiple stage use cases together for users. This helps users mature in their DevOps journey. 
* Smaller organizations do not have the resources to hire a central platform team. GitLab makes a compelling case to these companies by offering an out-of-the-box experience.
* Auto DevOps can be a big differentiator. GitLab is the most comprehensive value delivery platform. Auto DevOps enables it all to work together for users easily. Yet it should be flexible enough to allow advanced users customization.
* GitLab can add additional hosted services for GitLab customers, such as review apps run from GitLab infrastructure. This allows customers to gain more value without having to provision additional development infrastructure and build more development workflows.

## Key Capabilities for Deployment

Enterprises are increasingly choosing to have a [multi-cloud strategy](https://gitlab.com/gitlab-com/Product/-/issues/2287#note_526573920). Furthermore, with the increasing adoption of microservices architecture and infrastructure as code, traditional release tools are inadequate. This, coupled with the traditional deployment requirement of governance, compliance, and security, means that deployment tools have to meet a high bar and address a set of evolving technical and organizational requirements.

* **Multi-cloud and hybrid cloud:** For organizations adopting a multi-cloud strategy, they need their deployment tools to enable them to deploy to the targets of their choice, regardless of which cloud they choose. 
* **Everything as code:** - Deployment tooling, including pipelines, infrastructure, environments, and monitoring tools, are constantly involving. If they can be stored as code and version controlled, it will enable organizations to more effectively collaborate and avoid costly mistakes.
* **GitOps:** [GitOps](https://about.gitlab.com/topics/gitops/) enables organizations to more effectively operate and manage their infrastructure.
* **Environment management:** Organizations typically operate multiple environments, each serving different needs. Deployment tools should help managing environments, a related concept to deployment, easy and intuitive.
* **Monitoring/Observability:** Having the confidence that your deployments are successful is critical to enabling continuous deployment. Without the ability to understand the health and performance of environments, teams will not be able to deploy frequently.
* **Feedback/Reporting:** Measuring and reporting on deployment and overall DevOps performance is critical for communicating ROI and identifying improvement areas.
* **Quality gates:** Organizations need to control what can be deployed. Enabling reviews and approvals built right into the deployment workflow is a critical requirement. The ability to perform automatic rollback, environment freeze, scaling deployment also enables organizations to be more in control.
* **Progressive delivery:** Given the complexity of modern applications, teams need granular control on what gets deployed to specific audiences.

## Strategy

In Deployment, we are targeting [Priyanka (Platform Engineer)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer) as our primary persona. She is responsible for setting up the systems her company's development teams use to develop, test, **ship**, and **operate** their applications. It is likely she works at an enterprise where there is a mix of deployment targets. She also faces the challenges of ever-increasing complexity; as more contributors, more services, more infrastructure, more databases, and a mix of technologies are added to her applications. She is looking for ways to create a system to manage the complexity.

We plan to iteratively create a solution by focusing on two strategic vectors. 

First, we want to make it easy to deploy to Kubernetes. Kubernetes is growing fast and also is a short-cut around building specific integrations with cloud vendors. Kubernetes is an efficient target for deployment. It is also where deployment innovation is happening fastest. It is important that we [mitigate this low-end disruption](/direction/#mitigating-low-end-disruption) and make deployments to Kubernetes that work by default. 

Second, we want to make it easy for Priyanka to know she is doing a good job. GitLab is uniquely positioned as a solution to visualize how well teams are shipping software. Helping her in this way enables her to focus on the next thing to enable her customers, the development teams. It also helps her argue for more investment from her leadership, which leads to bringing more customers to GitLab.

By executing on this sandwich strategy, and leveraging the fact that the GitLab CI/CD is the default pipeline workflow for development teams already today, we aim to make adoption of additional deployment features more efficient.

### What we aren't focused on now

#### SMBs
The majority of smaller companies with singular deployment targets and simpler collaboration concerns do not need anyone to act as a Platform Engineer. We believe that setting sane defaults for GitLab's deployment features will fit their needs.

#### Cloud-native
Distinguishing from [Kubernetes-native](https://cloudark.medium.com/towards-a-kubernetes-native-future-3e75d7eb9d42), which is our initial focus area. We will not be focused on other cloud-native solutions, such as Docker Swarm, Apache Mesos, and Amazon ECS, because they're not nearly as successful as kubernetes.

#### Building more CD templates
Actively adding templates or improving existing templates is not our main focus. The CI/CD pipeline is flexible and enables GitLab users to create what they need as is. It may be worthwhile to examine how we can enable the wider GitLab community to share and reuse similar templates and treat them as lego blocks that can be adopted and put to use quickly. However, it is not our focus right now.

### Other considerations

1. Progressive delivery and advanced deployment features are important to enable Priyanka to control complex deployment scenarios.  
1. Environments as a first class concept within GitLab allows teams to have granular control for multiple and shared environments between projects and groups, which makes managing deployments more simple.

### What's next

- Release Group - [What's Next for CD](https://about.gitlab.com/direction/release/continuous_delivery/#whats-next--why)
- Configure Group - [What's Next](https://about.gitlab.com/direction/configure/#opportunities)

Beyond Release and Configure, Monitor is also an important aspect of operating your production deployments. We will be adding direction content on this topic.

Lastly, within GitLab, the nomenclature for deployments, environments, release, delivery are often inconsistent, interchangeable, and confusing. We need to settle on a standard and build it into our products and documentation.

## Jobs To Be done

To be added

## Competitive Landscape

<%= partial("direction/ops/competition/github-actions") %>

<%= partial("direction/ops/competition/harness") %>

<%= partial("direction/ops/competition/spinnaker") %>

<%= partial("direction/ops/competition/waypoint") %>

<%= partial("direction/ops/competition/argo-cd") %>

<%= partial("direction/ops/competition/weaveworks") %>

<%= partial("direction/ops/competition/jenkins-x") %>

<%= partial("direction/ops/competition/octopus-deploy") %>

