---
layout: markdown_page
title: "Group Direction - Project Management"
---

- TOC
{:toc}

## 1 Year Plan

A Brief Sidenote: This is written in the format of an [internal press release](https://medium.com/bluesoft-labs/try-an-internal-press-release-before-starting-new-products-867703682934) describing the finished state of Project Management functionality at the end of the corresponding period. It is subject to change based on feedback from internal stakeholders and the wider community. It's goal is to act as a compass as we continue to build and deliver value in an iterative and incremental manner.

### GitLab Launches Innovative Team Planning Capabilities

[Rant](https://devrant.com/rants/520732/them-we-should-totally-use-jira-me-why-its-sucks-so-hard-them-i-like-it-because) no more. There’s finally a set of unified planning tools that empower modern agile teams to continuously deliver value.

#### Summary

66% of enterprise software projects have overruns. Our robust planning and project management tools provide unparalleled capabilities to help teams break down silos, maintain alignment, and collaborate effectively to release value driven software on time and on budget. No integrations required.

#### Problem

Let’s be honest. The state of project management software is underwhelming. The market is littered with fragmented, overly complicated project management tools that are [slow, cumbersome](http://log.liminastudio.com/writing/commentary/why-i-finally-ditched-jira), and prioritize enforced workflows over facilitating meaningful collaboration. Most are [not organized](https://news.ycombinator.com/item?id=18985487) around how teams actually prefer to plan and track work; and require advanced configuration and numerous integrations with third-party services before they can provide the level of traceability and compliance that enterprises require. Up until this point, we were also [contributing to this problem](https://news.ycombinator.com/item?id=14353746).

#### Solution

GitLab now has first class support for the majority of agile methodologies; enabling cross-functional teams to collaborate in real time and make tangible progress towards mastering [agile fluency](https://martinfowler.com/articles/agileFluency.html) while simultaneously releasing great software -- all within the industry leading unified DevOps platform. Instead of relying on static charts and reports to help teams make better commitments, we’ve gone a step further to surface meaningful actions and insights directly within the appropriate collaborative contexts so teams can stay aligned and make better decisions without breaking flow. Team level projects seamlessly roll up into the portfolio level, providing visibility, meaningful reporting, and transparency across the entire organization. We’ve made our planning tools highly extensible to support each team’s unique needs, yet extremely simple to adopt by leveraging popular conventions so your team can begin planning their next release with just a few clicks.

> “Our planning toolkit is the culmination of hundreds of conversations with our customers, years of hard work, and thousands of contributions from the wider community. We still have a long way to go to achieve how we envision teams collaborating within GitLab, but our customers are reporting dramatic improvements in being able to ship customer and business value on time and on budget, which is a strong indicator that we’re on the right track.”  -- Gabe Weaver @ Senior Product Manager @ GitLab  

If you’re already using GitLab, then getting started is as simple as using Issues and Issue Boards. If you’re migrating from another project management tool like Jira, we’ve got you covered with a robust set of importers to help make the switch a breeze.

> “I was a little hesitant about using GitLab for project management, but after witnessing my development teams stage a walk-out to convince upper management that saying goodbye to JIRA was long overdue, I realized just how far GitLab has come.”  -- Executive @ Enterprise

We’re on a mission to make it easy for everyone to contribute. Want to learn more about our planning tools and if they’ll be a good fit for your team’s needs? Checkout the FAQ below or reach out to start a discussion.

#### FAQ

##### Can you provide a more detailed breakdown of the improvements you've made?

Absolutely. Here is a rough outline of how the planning toolset has evolved:

- Teams can collaborate in real time on Issues and Issues Boards.
- Issue Boards now natively support most continuous and timeboxed agile methodologies; and have been revamped to provide more contextual information and reports that teams need when planning iterations/sprints and releases.
- We’ve introduced the ability for Groups to use an extensible workflow engine to automate and measure various lifecycle stages for issues and merge requests; which can be applied to Issue Boards across all of your sub-groups and projects with the click of a button.
- Epics have been overhauled and are thoughtfully integrated into Issue Boards, providing an intuitive experience for understanding how a team’s day to day work on the project level fits into the bigger picture portfolio roadmap.
- Labels can now be part of a Label Set, which exposes some advanced capabilities for working with custom fields. We’ve also addressed a lot of the feedback regarding label management and made it easier to follow some best practices regarding label usage across groups and projects.
- We’ve made numerous UX improvements across the board such as the ability to interact with Issues and Epics from anywhere within the application without having to leave your current context and workflow.
- The user interface has been overhauled to provide massive responsiveness and performance gains.
- To-do items have been replaced with a unified notification and task management dashboard. You can safely say goodbye to using email to triage your Issue management and workflow.
- Issues have been complemented with first class support for Tasks. Tasks enable cross-functional teams to easily break down, estimate, and assign issue level work items.
- Issues now also have first class support for Types, which opens the door for advanced templating and customizations to better tailor Issues for different kinds of workflows.
- There are significant improvements to time tracking and reporting. Individuals can now start and stop timers from anywhere within GitLab and more easily attach time estimates to issues.

##### What do you mean by "collaborate in real time"?

Remember the first time you ever opened a Google Doc and saw a bunch of people typing at the same time? Anyone viewing the same issue that has the proper authorization can simultaneously work on the description. If an issue moves lists on an Issue Board, you’ll immediately see that change reflected without needing to refresh the page. Pretty awesome right?

##### Timeboxed and continuous agile methodologies? Please explain...

There are lots of different ways that teams organize and manage their work. Methodologies like Scrum and ExtremeProgramming use timeboxes as anchors to manage sprints and iterations. At the end of each timebox, they reflect on the work they’ve completed and use metrics like velocity and volatility to help make informed decisions on what to commit to for the next timebox.

Other popular methodologies like Kanban leverage a continuous flow model and use concepts like work in progress limits, lead time, and cycle time to measure effectiveness and efficiency of the development process.

Issue Boards now have first class support for these types of methodologies, including special modes for quicky refining issues and prioritizing your backlog. Additionally, there are a wide variety of critical reports integrated directly with Issue Boards such as improved burndown charts, historical velocity and volatility, and cumulative flow diagrams among many others. =

##### Can you tell me more about this rules engine?

It’s still in its infancy, but the long term vision is to allow you to configure advanced workflow automations based on where an Issue or Merge Request is in its lifecycle. For now, you’re able to define customizable stages that map to the lifecycle of important artifacts within GitLab like Issues and Merge Requests. Instead of manually dragging Issues from one list to another within an Issue Board, you can define conditions that will automatically advance an Issue to the next step in your workflow.

##### How are label sets the same thing as custom fields?

A series of labels can now be grouped together into a set. The set has a name. There are some system level label sets like “Issue Type” that you cannot delete, but you can also create as many label sets as you want. Label sets are included on Issues and Merge Requests, but show up as distinct fields and are not clustered with other labels. Label sets can be configured to allow only one label value to be applied at a given time (like how scoped labels works now), multiple label values, priority order, directional order, and several additional options. We may also just add straight up custom fields at some point ;)

##### Can you tell me more about the UX improvements and how you improved the performance so dramatically?

Startups are fast paced and in order to get traction, sometimes you have to carry a bit of technical debt for a while. A year ago, our frontend architecture was a cobbled together mess of Vue, HAML/HTML, and jQuery. As the defect rate and response times increased, we knew It was time to pay the bank, so we refactored the majority of the frontend to follow consistent patterns based on a reusable component library we built called [Pajamas](https://design.gitlab.com). We also transitioned to exclusively using our shiny new GraphQL API. These are a few of the things, that when combined, provided for opened the door for new ways to act with core artifacts within GitLab in a much more intuitive way.

##### Unified notifications and tasks? Can I still use email if I want?

Absolutely. We also made some improvements that allow you to have more control over how email notifications work. While we don’t want to force you to change your workflow, context switching is a [productivity killer](https://blog.trello.com/why-context-switching-ruins-productivity). That’s why we designed our unified notifications and tasks dashboard to help you facilitate meaningful communications, interact with issues as necessary, and provide a way for you to prioritize all of the things you need to get done across your projects. We’re pretty sure that once you give it a try, you won’t want to go back to using email ever again ;) 
