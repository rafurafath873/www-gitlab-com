---
layout: markdown_page
title: "Monitor workflow - Instrument"
description: "In order to know the health of your services and application, GitLab will help you collect appropriate application and infrastructure data. Learn more here!"
canonical_path: "/direction/monitor/apm/workflow/instrument/"
---

- TOC
{:toc}

# Instrument

This page gives an overview of GitLab's vision for enabling the **Instrument** workflow within the Monitor stage.

## Why Instrument?

The first step in application performance management is collecting the proper measurements. Getting accurate data in is critical. In order to know the health of your services and application, we'll help you collect appropriate application and infrastructure data. We focus primarily on instrumenting metrics and traces as apart of the [three pillars of observability](https://www.oreilly.com/library/view/distributed-systems-observability/9781492033431/ch04.html), logs will be streamed directly from a deployed Elasticsearch as mention the [logging vision](https://about.gitlab.com/direction/monitor/apm/logging/) section

* Infrastructure and application **metrics** will be collected by [Prometheus](https://prometheus.io/)
* Application **traces** will be collected by [Jaeger](https://www.jaegertracing.io/) agent 
* **Logs** will be collected using [Elasticsearch](https://www.elastic.co/)

Doing so will enable you to act based on known information.

<iframe width="560" height="315" src="https://www.youtube.com/embed/tuI2oJ3TTB4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Initial setup

We aspire to make the configuration and instrumentation of these easy and automated as possible. We do this by providing a clear documented guide for the following steps:

* Deploy managed apps (Elasticsearch, Prometheus, and Jaeger) - to start collecting logs, metrics, and traces.
* Instrument your application to collect metrics and traces
* Confirm the data is getting ingested into the solution
* Assist in categorizing your data appropriately with labels and tags

## Dashboards

Dashboards are a vital component in any observability solution as they provide the key to understanding complex data streams. A user should be able to view and set up the following dashboards:

*  Business metrics dashboards
*  Infrastracture related dashboards
*  Customized dashboards for ad-hoc interaction with time series information 
*  Logging UI for log exploration
*  Traces UI for visualizing traces and identifying bottlenecks 

## Define Business level Objectives

The detection of unwanted behaviors of an application starts with setting Service Level Objectives and Indicators. These are internal standards of success by which we measure production systems. Monitoring and alerting on these critical metrics is the basis of any observability solution. Businesses want to know the health of these metrics rather than details about the underlying infrastructure. In order to bridge that gap, users should be able to define the business metrics (SLI/SLO) based on application or infrastructure metrics. This way, when an alert is triggered there is a common understanding of its impact.

## What's next

We plan to provide a streamline instrumentation experience to allows our users to quickly and easily collect the needed information, additional detail on our direction can be found in the [instrument to minimal](https://gitlab.com/groups/gitlab-org/-/epics/2316) epic

