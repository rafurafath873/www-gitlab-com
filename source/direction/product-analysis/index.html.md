---
layout: markdown_page
title: Product Direction - Product Analysis
canonical_path: "/direction/product-analysis/"
---

## Product Analysis Group Overview

The Product Analysis group consists of a team of product analysts. This group reports to the Director of Product, Growth and serves as a specialized analytics team to support product data analysis requests. The primary customer this group serves is Product Managers from various sections/groups. 

### Product Analysis Group members

* Manager, Product Data Analysis: TBH
* Dave Peterson: Senior Product Analyst
* Nicole Galang: Product Analyst 
* Product Analyst: TBH 

The Product Analysis group is part of the [Product Analytics Fusion Team](https://about.gitlab.com/handbook/business-ops/data-team/organization/#release-to-adoption) where the product analysts work closely with members from the central data team, including:   
* Mathieu Peychet: Senior Data Analyst
* Kathleen Tam: Staff Data Analyst 

## Responsibilities 

Since the Product Analysis group is a specialized analytics team, it collaborates with the [Data team](https://about.gitlab.com/handbook/business-ops/data-team/) and [Product Intelligence group](https://about.gitlab.com/direction/product-intelligence/) very closely. Here is a quick summary of the responsbilities of each of these 3 teams: 

| Team Name | DRI | Goal | Example Tasks |
| ---------- | ---------- |-------------- |-------------- |
|Product Analysis group|Manager, Product Data Analysis (TBH)| Perform analysis to unlock product insights when the data is available| Ad hoc product analysis, experiment analysis, Product KPI tracking, and creation and maintenance of standardized Product metric dashboards|
|Product Intelligence group|Keanon O’Keefe: Senior PM, Product intelligence| Define and improve capabilities of product data set|Make sure our product data set is comprehensive, available, and accurate,and that team members are enabled to create new instrumentation, charts, and dashboards in a self-service fashion|
|Data Team|Rob Parker, Senior Director, Data and Analytics| Build and maintain product data models and infrastructure to support data analysis|Create repeatable and reliable product data sets & models & related data pipelines/processes|

Note that due to the strategic importance of product data analysis and how early we are in the journey, the data team and product analysis team formed a Product Analytics Fusion Team. The fusion team will take on some shared responsibilities in data analysis, data modeling and knowledge transfer/training for the foreseeable future, to progress our product data capabilities.  

For more information about how the Product Analysis group works with the ata team at GitLab, please refer to following the model below and the [data team's handbook page](https://about.gitlab.com/handbook/business-ops/data-team/#data-teams-at-gitlab)
 
### High level Collaboration Workflow between the 3 teams
1. A data question needs to be answered with product data, the requester opens a data issue
2. The data triager decides whether we have the data avaliable to answer that question  
3. If we don't have the data, open an issue for Product Intelligence Group
4. If we have the data, route the requests to the Product Analysis Group
5. Product Analysts perform analysis and build draft charts or dashboards
6. Product Analysts define if the data analysis needs to be repeatable
7. If the data analysis needs to repeatable, open an issue for data team to create trusted data model to enable future analysis


## Intake Process

### Issue Submission Process
1. For all product analysis requests, please create an issue on [data project](https://gitlab.com/groups/gitlab-data/-/issues), and add a “product analysis” label and your section and group labels. If the issue is for an experiment, use the Experiment issue template([view template](https://gitlab.com/gitlab-data/analytics/-/blob/master/.gitlab/issue_templates/experiment_template.md)) within the GitLab Data Team project and fill in all relevant fields.
2. All data issues with “product analysis” labels will appear on a [product analysis board](https://gitlab.com/gitlab-data/analytics/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=product%20analytics)
3. In the issue, please explain the urgency/criticality of the requests and provide as much context as possible
5. The Director of Product Growth will help prioritize the work based on importance/capacity and work closely with the Chief Product Officer and VP, Product Management on trade-offs if needed 


### Issue Prioritization Process (To be completed by the Product Analysis team)
1. The product analysis group works on a 2-week milestone cadence  
2. Prioritization to be set during the planning meeting the week before the start of the new milestone
3. Prioritization labels will be added to reflect the issue's impact and urgency
4. Issue will be assigned an estimated projected time commitment
5. New issues will be considered alongside existing issues to set prioritization for the upcoming milestone, adjusting task load based on past milestone performance.


## Capacity
As the Product Analysis team is still fairly new and small, we expect most PMs to self-serve analysis and only create data issues on critical asks

## Slack
1. data-product-analytics-fusion-team - For the Product Analytics Fusion Team
1. data - For general data questions 

## Product Analysis Group’s Current Focus Area

As the Product Analysis Group was established in FY 21 Q4, the team is still in the process of forming. In FY22 Q1 and Q2, we will focus on the following areas: 

### Analysis Tasks 
* Knowledge transfer from the Data team to the Product Analysis group on product KPIs such as SpO and xMAUs
* Work with the Data team to understand and improve data models powering product KPIs 
* Perform ad-hoc analysis to support product analysis requests, including: 
  - Product KPIs: SpO & xMAU
  - Product Key Review & executive/OKR support
  - Growth Experiments Analysis & New User Conversion Analysis

### Team and Process
* Hire for the open positions and get new product analysts onboarded 
* Understand team velocity and establish and improve the prioritization process 
* Establish collaboration process with the data team and product intelligence group 

## Helpful resources & links
1. [Data for product managers](https://about.gitlab.com/handbook/business-ops/data-team/programs/data-for-product-managers/)
2. [Data team How we can help you Page](https://about.gitlab.com/handbook/business-ops/data-team/#how-we-can-help-you)  

