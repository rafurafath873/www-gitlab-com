require 'spec_helper'
require_relative '../../helpers/custom_helpers'
require_relative '../../helpers/team_page_helpers'

describe TeamPageHelpers do
  class TemplateMock
    extend CustomHelpers
    extend TeamPageHelpers

    def self.tag(*args); end

    def self.signed_periscope_url(data)
      "#{data[:filters].first[:value]}:#{data[:dashboard]}:#{data[:chart]}"
    end
  end

  let(:default_html_opts) { { width: '100%', height: 300 } }
  let(:html_options) { { height: 100, width: '50%' } }
  let(:kpi_dash_id) { 570334 }

  describe '.team_sisense_chart' do
    context 'when html_options are given' do
      let(:options) do
        {
          html_options: html_options,
          dashboard_id: 123,
          chart_id: 456,
          team_id: 'plan_portfolio_management_be'
        }
      end

      it 'creates embed tag with options and src' do
        expected_args = [
          :embed,
          html_options.merge(src: 'plan_portfolio_management_be:123:456')
        ]
        expect(TemplateMock).to receive(:tag).with(*expected_args)

        TemplateMock.team_sisense_chart(**options)
      end
    end
    context 'when html_options are omitted' do
      let(:options) do
        {
          dashboard_id: 123,
          chart_id: 456,
          team_id: 'plan_portfolio_management_be'
        }
      end

      it 'creates embed tag with options and src' do
        expected_args = [
          :embed,
          default_html_opts.merge(src: 'plan_portfolio_management_be:123:456')
        ]

        expect(TemplateMock).to receive(:tag).with(*expected_args)

        TemplateMock.team_sisense_chart(**options)
      end
    end
  end

  describe '.team_mr_rate_chart' do
    let(:options) { { team_id: 'plan_portfolio_management_be' } }

    it 'uses the correct dashboard and chart ids' do
      expected_args = [
        :embed,
        default_html_opts.merge(src: "#{options[:team_id]}:#{kpi_dash_id}:7432827")
      ]

      expect(TemplateMock).to receive(:tag).with(*expected_args)

      TemplateMock.team_mr_rate_chart(**options)
    end
  end

  describe '.team_mean_time_to_merge_chart' do
    let(:options) { { team_id: 'plan_portfolio_management_be' } }

    it 'uses the correct dashboard and chart ids' do
      expected_args = [
        :embed,
        default_html_opts.merge(src: "#{options[:team_id]}:#{kpi_dash_id}:7433451")
      ]

      expect(TemplateMock).to receive(:tag).with(*expected_args)

      TemplateMock.team_mean_time_to_merge_chart(**options)
    end
  end

  describe '.team_mr_categories_chart' do
    let(:options) { { team_id: 'plan_portfolio_management_be' } }

    it 'uses the correct dashboard and chart ids' do
      expected_args = [
        :embed,
        default_html_opts.merge(src: "#{options[:team_id]}:#{kpi_dash_id}:7432830")
      ]

      expect(TemplateMock).to receive(:tag).with(*expected_args)

      TemplateMock.team_mr_categories_chart(**options)
    end
  end
end
